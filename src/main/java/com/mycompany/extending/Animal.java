package com.mycompany.extending;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */
public abstract class Animal {

    String eyeColor;
    String sex;
    
    /**
     * Abstract class Animal has eyeColor and sex values
     */
    
    
    public Animal()
    {
    this.eyeColor = "Brown";
    this.sex = "male";
    }

    public Animal (String eyeColor, String sex)
    {
    this.eyeColor = eyeColor;
    this.sex = sex;
    }
    
    /**
     *
     * @return eyeColor for animal
     */
    
    public String getEyeColor()
    {
    return eyeColor;
    }
    
    /**
     *
     * @param eyeColor sets eyeColor for Animal
     */
    
    public void setEyeColor(String eyeColor)
    {
    this.eyeColor = eyeColor;
    }
    
    /**
     *
     * @return sex of Animal
     */
    public String getSex()
    {
    return sex;
    }
    
    /**
     *
     * @param sex set sex of animal
     */
    
    public void setSex(String sex)
    {
    this.sex = sex;
    }
    
}