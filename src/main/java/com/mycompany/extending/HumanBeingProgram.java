package com.mycompany.extending;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */
public class HumanBeingProgram extends Animal
{

    /**
     *
     * @param args This is main method of this project
     */
    public static void main (String[] args)
  {
    HumanBeing me = new HumanBeing();
    output(me);
 
    HumanBeing you = new HumanBeing("blue", "female", "Jane Doe");
    output(you);
  }
 
  private static void output(HumanBeing human)
  {
    System.out.println(human.getName() + "'s eyes are: " + human.getEyeColor());
    System.out.println(human.getName() + " is " + human.getSex());
    System.out.println("--------------");
  }
}