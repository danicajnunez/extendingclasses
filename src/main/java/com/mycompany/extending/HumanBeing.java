package com.mycompany.extending;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */
class HumanBeing extends Animal {
    String name;
    
    public HumanBeing()
    {
    super();
    this.name = "John Doe";
    }
    
    
    
    public HumanBeing (String eyeColor, String sex)
    {
    super(eyeColor, sex);
    }
    
    //Super means that we want to call the constructor that is one step up in 
    //our inheritance chain. This means that Java will call the Animal constructor.
    
    public HumanBeing (String eyeColor, String sex, String name)
    {
    super(eyeColor, sex);
    this.name = name;
    }
    
    public String getName()
    {
    return name;
    }
    
    public void setName(String name)
    {
    this.name = name;
    }

   
    
}
