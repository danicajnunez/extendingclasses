package com.mycompany.extendingclasses;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */
//public class Car implements Vehicle {
//    
//
//    @Override
//    public Integer getNumberOfSeats()
//    {
//    return 5;
//    }
//    
//    @Override
//    public Integer getNumberOfWheels()
//    {
//    return 4;
//    }
//    
//    @Override
//    public String getVehicleType()
//    {
//    return "Car";
//    }
//    
//    public Integer getNumberOfDoors()
//    {
//    return 2;
//    }
//}

// vs. Abstract Class



public class Car extends Vehicle
{
public Car()
{
this.vehicleType = "Car";         //Constructor - The purpose of a constructor 
//in Java is to outline a section of code that will be executed when an Object 
//is first instantiated. So, this just means that when someone creates an instance
//of our Car Object, Java will automatically set the vehicleType to be "Car".
}

@Override
public Integer getNumberOfWheels()
{
return 4;
}

@Override
public Integer getNumberOfSeats()
{
return 2;
}

}
