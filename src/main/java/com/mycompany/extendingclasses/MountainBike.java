package com.mycompany.extendingclasses;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */
public class MountainBike extends Bicycle {

    /** the MountainBike subclass adds one field
     * seatHeight is height of seat
     */
    
    public int seatHeight;

    /**the MountainBike subclass has one constructor
     * 
     * @param startHeight height of seat
     * @param startCadence cadence of bike
     * @param startSpeed speed of bike
     * @param startGear gear of bike
     */
    
    
    public MountainBike(int startHeight,
            int startCadence,
            int startSpeed,
            int startGear) {
        super(startCadence, startSpeed, startGear);
        seatHeight = startHeight;
    }

    /**the MountainBike subclass adds one method
     * 
     * @param newValue is seat height
     */
    
    public void setHeight(int newValue) {
        seatHeight = newValue;
    }

}
