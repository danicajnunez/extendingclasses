package com.mycompany.extendingclasses;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */
public class Bicycle {

    public int cadence;
    public int gear;
    public int speed;
    

    /** the bicycle class has one constructor
     * 
     * @param startCadence number of revolutions of the crank per minute
     * @param startSpeed speed of bike
     * @param startGear gear of bike
     */
    
    
    public Bicycle(int startCadence, int startSpeed, int startGear) {
        gear = startGear;
        cadence = startCadence;
        speed = startSpeed;
    }

    /**
     * 
     * @param newValue sets Cadence 
     */
    public void setCadence(int newValue) {
        cadence = newValue;
    }

    /**
     * 
     * @param newValue sets Gear
     */
    
    public void setGear(int newValue) {
        gear = newValue;
    }
    
    /**
     * 
     * @param decrement decreases speed
     */

    public void applyBrake(int decrement) {
        speed -= decrement;
    }
    
    /**
     * 
     * @param increment increases speed
     */

    public void speedUp(int increment) {
        speed += increment;
    }

}
