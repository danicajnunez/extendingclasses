package com.mycompany.extendingclasses;






/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */

//public class Bus implements Vehicle {
//
//@Override
//public Integer getNumberOfSeats()
//{
//return 35;
//}
//
//@Override
//public Integer getNumberOfWheels()
//{
//return 6;
//}
//
//@Override
//public String getVehicleType()
//{
//return "Bus";
//}
//
//public Integer getNumberOfDoors()
//{
//return 4;
//}
//
//}

// vs. Abstract Class

public class Bus extends Vehicle
{
public Bus()
{
this.vehicleType = "Bus";         //Constructor - The purpose of a constructor 
//in Java is to outline a section of code that will be executed when an Object 
//is first instantiated. So, this just means that when someone creates an instance
//of our Car Object, Java will automatically set the vehicleType to be "Car".
}

@Override
public Integer getNumberOfWheels()
{
return 6;
}

@Override
public Integer getNumberOfSeats()
{
return 20;
}

}
