package com.mycompany.extendingclasses;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */
//public interface Vehicle {
//
//    public Integer getNumberOfSeats();
//
//    public Integer getNumberOfWheels();
//
//    public String getVehicleType();
//}
//
//vs. Abstract class  

    public abstract class Vehicle {

        public String vehicleType;

        /**
         * 
         * @return if vehicleType is Car, then number of seats is 5
         * @return if vehicleType is Bus, then number of seats is 20
         * @return if vehicleType is Motorcycle, then number of seats is 1
         * return null if no vehicle type is used
         */
        
        public Integer getNumberOfSeats() {
            if (this.vehicleType.equals("Car")) {
                return 5;
            } else if (this.vehicleType.equals("Bus")) {
                return 20;
            } else if (this.vehicleType.equals("Motorcycle")) {
                return 1;
            }
            return null;
        }

        /**
         * 
         * @return vehicleType as string
         */
        
        public String getVehicleType() {
            return this.vehicleType;
        }

        /**
         * 
         * @return Number of Wheels
         */
        
        public abstract Integer getNumberOfWheels();
    }

   
